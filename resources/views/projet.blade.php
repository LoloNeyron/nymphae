@extends('layouts.web')

@section('title', 'Projet')

@section('content')
    <div class="wrapper">
        {{--content --}}
        <div id="content" class="col-12">
            <div class="row p-4">
                <div class="col-12">
                    <div class="row pb-3">
                        <div class="col-6">
                            <button class="btn btn-primary" data-toggle="collapse" href="#formCreationProjet" role="button"
                                    aria-expanded="false" aria-controls="formCreationProjet">
                                Créer un nouveau projet
                            </button>
                        </div>
                        <div class="col-6">
                            <p class="nav justify-content-end">
                                <button type="button" class="btn btn-info sidebarCollapse">
                                    <i class="fas fa-search"></i>
                                    <span>Listes des projets</span>
                                </button>
                            </p>
                        </div>
                    </div>
                    <div class="collapse" id="formCreationProjet">
                        <div class="card card-body">
                            <h4>Nouveau projet : <span id="displayProjetName"></span></h4>
                            @if(\App\Models\client::first() === NULL)
                                <div class="alert alert-danger">
                                    <h5><i class="fas fa-exclamation-triangle"></i> Attention</h5>
                                    <p>Aucun client n'as été créer.</p>
                                    <p>Veuillez créer un client avant de créer un projet.</p>
                                </div>
                            @endif
                            <form action="{{Route('Projet--create')}}" method="GET">
                                @csrf
                                <small>ID : {{$lastId}}</small>
                                <br>
                                <div class="row">
                                    <div class="col-6">
                                        <label for="nameProjet">Nom</label>
                                        <input id="nameProjet" required type="text" name="name" class="form-control" required
                                               placeholder="Name">
                                    </div>
                                    <div class="col-6">
                                       @include('components.tool.select-clients')
                                    </div>
                                    <div class="col-12">
                                        <label for="descriptionProjet">Description</label>
                                        <textarea  class="form-control" name="description" id="descriptionProjet" required rows="3"></textarea>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-success btn-block">Enregistrer</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--end content --}}

        {{--sideSearch --}}
        <div class="nav p-3 bg-white shadow" id="sideSearchTab">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-8">
                        <h1>Liste des Projet :</h1>
                    </div>
                    <div class="col-4">
                        <p class="nav justify-content-end">
                            <button type="button" class="btn btn-info sidebarCollapse">
                                <i class="fas fa-times"></i>
                            </button>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        @include('components.projet.tableData')
                    </div>
                </div>
            </div>
        </div>
        {{--end sideSearch --}}
    </div>

@endsection
@push('script')
    <script>
        $('#nameProjet').on('keyup', function () {
            $('#displayProjetName').html($(this).val());
        })
    </script>
@endpush
