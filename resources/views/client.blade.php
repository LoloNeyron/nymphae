@extends('layouts.web')

@section('title', 'Clients')

@section('content')
    <div class="wrapper">
        {{--content --}}
        <div id="content" class="col-12">
            <div class="row p-4">
                <div class="col-12">
                    <div class="row pb-3">
                        <div class="col-6">
                            <button class="btn btn-primary" data-toggle="collapse" href="#formCreationClient"
                                    role="button"
                                    aria-expanded="false" aria-controls="formCreationClient">
                                Créer un nouveau client
                            </button>
                        </div>
                        <div class="col-6">
                            <p class="nav justify-content-end">
                                <button type="button" class="btn btn-info sidebarCollapse">
                                    <i class="fas fa-search"></i>
                                    <span>Listes des clients</span>
                                </button>
                            </p>
                        </div>
                    </div>
                    <div class="collapse" id="formCreationClient">
                        <div class="card card-body">
                            <h4>Nouveau Client : <span id="displayClientName"></span></h4>
                            <form action="{{Route('Client--create')}}" method="GET">
                                @csrf
                                <small>ID : {{$lastId}}</small>
                                <br>
                                <div class="row">
                                    <div class="col-12">
                                        <label for="nameClient">Nom</label>
                                        <input id="nameClient" required type="text" name="name" class="form-control"
                                               placeholder="Name">
                                    </div>
                                    <div class="col-12">
                                        <label for="descriptionClient">Description</label>
                                        <textarea class="form-control" name="description" id="descriptionClient"
                                                  rows="3" required></textarea>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-success btn-block">Enregistrer</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--end content --}}

        {{--sideSearch --}}
        <div class="nav p-3 bg-white shadow" id="sideSearchTab">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-8">
                        <h1>Liste des Clients :</h1>
                    </div>
                    <div class="col-4">
                        <p class="nav justify-content-end">
                            <button type="button" class="btn btn-info sidebarCollapse">
                                <i class="fas fa-times"></i>
                            </button>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        @include('components.client.tableData')
                    </div>
                </div>
            </div>
        </div>
        {{--end sideSearch --}}
    </div>

@endsection

@push('modal')
    @foreach($data as $d)
        <div class="modal fade" id="modalEditClient{{$d->id}}" tabindex="-1" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modification du client : {{$d->name}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{Route('Client--update')}}" method="GET">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <input type="hidden" name="id" value="{{$d->id}}">
                                    <label for="nameClient">Nom</label>
                                    <input id="nameClient" required type="text" name="name" class="form-control"
                                           placeholder="Name" value="{{$d->name}}">
                                </div>
                                <div class="col-12">
                                    <label for="descriptionClient">Description</label>
                                    <textarea class="form-control" name="description" id="descriptionClient" rows="3"
                                              required>{{$d->description}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-portal-exit"></i> Annuler</button>
                            <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Enregister</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endpush

@push('script')
    <script>
        $('#nameClient').on('keyup', function () {
            $('#displayClientName').html($(this).val());
        })
    </script>
@endpush
