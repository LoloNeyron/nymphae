@extends('layouts.webMobile')

@section('title', 'Course')

@section('content')
    <div class="col-12 text-center text-white">
        @include('components.application-logo', ['attributes' => 'style=display:block;margin:auto;width:25vw;color:rgba(75,60,98);fill:currentColor;height:25vw;max-width:300px;max-height:300px'])
        <h5 class="h5 text-center text-white ct-font light-font-weight capitalize">NYMPHAE</h5>
    </div>
    <div class="col-12 text-center text-white">
    <hr>
    <h1 class="light-font-weight py-3">Course</h1>
    <hr>
    <div class="row">
        <div class="col-12">
            <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#ModalNewListe">Nouvelle Liste</button>
        </div>
        <div class="col-12">
            <br>
            <hr>
            <br>
            <table class="table table-hover table-dark table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($lcs as $lc)
                        <tr>
                            <td><a href="/lc/{{$lc->id}}">{{$lc->name}}</a></td>
                            <td><a href="/lc/{{$lc->id}}">{{Jenssegers\Date\Date::parse($lc->created_at)->format('l j F Y')}}</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('modal')
        <div class="modal fade" id="ModalNewListe" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nouvelle liste de courses</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{Route('newlc')}}" method="GET">
                        <div class="modal-body">
                            @csrf
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <div class="form-group">
                                <label for="name">Nom</label>
                                <input type="text" name="name" class="form-control" id="name">
                            </div>
                            <div class="form-group">
                                <label for="desc">Description</label>
                                <textarea class="form-control" name="desc" id="desc" cols="30" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                            <button type="submit" class="btn btn-primary">Créer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endpush
