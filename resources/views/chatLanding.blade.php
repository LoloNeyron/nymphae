@extends('layouts.web')

@push('header')
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,300;0,400;0,700;1,100;1,300;1,400;1,700&display=swap" rel="stylesheet">
    <style>
        .chat-font {
            font-family: 'Work Sans', sans-serif;
        }
    </style>
@endpush

@section('title', 'Chat')

@section('content')
    <div class="container-fluid">
        <div class="row p-4">
            <div class="col-12">
                <h1>Chat</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-1 rounded bg-grey-light">
                <div class="row overflow-auto py-3">
                    <div class="col-12 text-center">
                        <img class="convers-bubble" data-toggle="tooltip" data-placement="top" title="Tooltip on right" src="https://eu.ui-avatars.com/api/?size=50&background=111&color=fff&name=Loic+Neyron" alt="">
                    </div>
                    <div class="col-12 text-center">
                        <img class="convers-bubble" data-toggle="tooltip" data-placement="top" title="Tooltip on right" src="https://eu.ui-avatars.com/api/?size=50&background=111&color=fff&name=Loic+Neyron" alt="">
                    </div>
                    <div class="col-12 text-center">
                        <img class="convers-bubble" data-toggle="tooltip" data-placement="top" title="Tooltip on right" src="https://eu.ui-avatars.com/api/?size=50&background=111&color=fff&name=Loic+Neyron" alt="">
                    </div>
                    <div class="col-12 text-center">
                        <img class="convers-bubble" data-toggle="tooltip" data-placement="top" title="Tooltip on right" src="https://eu.ui-avatars.com/api/?size=50&background=111&color=fff&name=Loic+Neyron" alt="">
                    </div>
                    <div class="col-12 text-center">
                        <img class="convers-bubble" data-toggle="tooltip" data-placement="top" title="Tooltip on right" src="https://eu.ui-avatars.com/api/?size=50&background=111&color=fff&name=Loic+Neyron" alt="">
                    </div>
                    <div class="col-12 text-center">
                        <img class="convers-bubble" data-toggle="tooltip" data-placement="top" title="Tooltip on right" src="https://eu.ui-avatars.com/api/?size=50&background=111&color=fff&name=Loic+Neyron" alt="">
                    </div>
                    <div class="col-12 text-center">
                        <img class="convers-bubble" data-toggle="tooltip" data-placement="top" title="Tooltip on right" src="https://eu.ui-avatars.com/api/?size=50&background=111&color=fff&name=Loic+Neyron" alt="">
                    </div>
                    <div class="col-12 text-center">
                        <img class="convers-bubble" data-toggle="tooltip" data-placement="top" title="Tooltip on right" src="https://eu.ui-avatars.com/api/?size=50&background=111&color=fff&name=Loic+Neyron" alt="">
                    </div>
                    <div class="col-12 text-center">
                        <img class="convers-bubble" data-toggle="tooltip" data-placement="top" title="Tooltip on right" src="https://eu.ui-avatars.com/api/?size=50&background=111&color=fff&name=Loic+Neyron" alt="">
                    </div>
                    <div class="col-12 text-center">
                        <img class="convers-bubble" data-toggle="tooltip" data-placement="top" title="Tooltip on right" src="https://eu.ui-avatars.com/api/?size=50&background=111&color=fff&name=Loic+Neyron" alt="">
                    </div>
                    <div class="col-12 text-center">
                        <img class="convers-bubble" data-toggle="tooltip" data-placement="top" title="Tooltip on right" src="https://eu.ui-avatars.com/api/?size=50&background=111&color=fff&name=Loic+Neyron" alt="">
                    </div>
                    <div class="col-12 text-center">
                        <img class="convers-bubble" data-toggle="tooltip" data-placement="top" title="Tooltip on right" src="https://eu.ui-avatars.com/api/?size=50&background=111&color=fff&name=Loic+Neyron" alt="">
                    </div>
                </div>
            </div>
            <div class="col-10 rounded bg-grey-light ml-2" style="position: relative">
                <div class="p-2">
                    <h3 class="chat-font">ELS</h3>
                    <div class="row">
                        <div class="col-12 rounded bg-grey-lighter">
                            <div class="row">
                                <div class="chat-bulle-them ml-2">
                                    bla fshkfjsd sfkdjh ksdjfhksd
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-me ml-auto mr-2">
                                    <p>fshkfjsd sfkdjh ksdjfhksd</p>
                                    <p>fshkfjsd d</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-me ml-auto mr-2">
                                    <p>fshkfjsd d</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-them ml-2">
                                    bla fshkfjsd sfkdjh ksdjfhksd
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-me ml-auto mr-2">
                                    <p>fshkfjsd sfkdjh ksdjfhksd</p>
                                    <p>fshkfjsd d</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-me ml-auto mr-2">
                                    <p>fshkfjsd d</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-them ml-2">
                                    bla fshkfjsd sfkdjh ksdjfhksd
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-me ml-auto mr-2">
                                    <p>fshkfjsd sfkdjh ksdjfhksd</p>
                                    <p>fshkfjsd d</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-me ml-auto mr-2">
                                    <p>fshkfjsd d</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-them ml-2">
                                    bla fshkfjsd sfkdjh ksdjfhksd
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-me ml-auto mr-2">
                                    <p>fshkfjsd sfkdjh ksdjfhksd</p>
                                    <p>fshkfjsd d</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-me ml-auto mr-2">
                                    <p>fshkfjsd d</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-them ml-2">
                                    bla fshkfjsd sfkdjh ksdjfhksd
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-me ml-auto mr-2">
                                    <p>fshkfjsd sfkdjh ksdjfhksd</p>
                                    <p>fshkfjsd d</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-me ml-auto mr-2">
                                    <p>fshkfjsd d</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-them ml-2">
                                    bla fshkfjsd sfkdjh ksdjfhksd
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-me ml-auto mr-2">
                                    <p>fshkfjsd sfkdjh ksdjfhksd</p>
                                    <p>fshkfjsd d</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="chat-bulle-me ml-auto mr-2">
                                    <p>fshkfjsd d</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center" style="position: absolute;bottom:5px;width: 100%;">
                    <div class="col-10" style="height: 100%">
                        <div class="form-group">
                            <input type="text" class="form-control" id="messageChat" aria-describedby="Message">
                        </div>
                    </div>
                    <div class="col-2" style="height: 100%">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-level btn-block">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endpush
