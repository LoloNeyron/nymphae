{{--@include('components.web.input', ['label' => 'entrer un perso ici', 'col' => 3, "id" => "test", "attr" => 'required', "type" => 'text', "name" => "perso", "placeholder" => "peso"])--}}
<div class="col-{{$col}}">
    <label for="{{$id}}">{{$label}}</label>
    <input id="{{$id}}" {{$attr}} type="{{$type}}" name="{{$name}}" class="form-control"
           placeholder="{{$placeholder}}">
</div>
