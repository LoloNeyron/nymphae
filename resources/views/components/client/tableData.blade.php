<table class="table table-hover table-striped rounded border-light text-center">
    <thead>
    <tr>
        <th>Nom</th>
        <th>Description</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $d)
        <tr>
            <td>{{$d->name}}</td>
            <td>
                <div>{{$d->description}}</div>
            </td>
            <td>
                <button class="btn btn-outline-info" data-toggle="modal" data-target="#modalEditClient{{$d->id}}"><i class="fas fa-pen"></i></button>
                <form action="{{route('Client--delete')}}" method="GET" class="d-inline">
                    <input type="hidden" name="id" value="{{$d->id}}">
                    <button type="submit" class="btn btn-outline-danger"><i
                            class="fas fa-trash"></i></button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
