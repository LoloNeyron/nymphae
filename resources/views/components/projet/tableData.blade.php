<table class="table table-hover table-striped rounded border-light text-center">
    <thead>
    <tr>
        <th>Nom</th>
        <th>Descriton</th>
        <th>Client</th>
        <th>date de création</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $d)
        <tr>
            <td>
                <div>{{$d->name}}</div>
            </td>
            <td>
                <div>{{$d->description}}</div>
            </td>
            <td>
                <div>{{\App\Models\client::where('id', $d->id_client)->first()->name}}</div>
            </td>
            <td>
                <div>{{$d->created_at}}</div>
            </td>
            <td>
                <button class="btn btn-outline-info"><i class="fas fa-pen"></i></button>
                <form action="{{route('Projet--delete')}}" method="GET" class="d-inline">
                    <input type="hidden" name="id" value="{{$d->id}}">
                    <button type="submit" class="btn btn-outline-danger"><i
                            class="fas fa-trash"></i></button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
