<label for="idClient">Client associé</label>
<select id="idClient" required type="text" name="idClient" class="form-control"
        placeholder="Name">
    <option disabled selected value="">Choisir un client</option>
    @foreach($clients as $client)
        <option value="{{$client->id}}">{{$client->name}}</option>
    @endforeach
</select>
