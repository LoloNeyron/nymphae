@extends('layouts.webMobile')

@section('title', 'Course')

@section('content')
    <div class="col-12 text-center text-white">
        @include('components.application-logo', ['attributes' => 'style=display:block;margin:auto;width:95vw;color:rgba(75,60,98);fill:currentColor;height:95vw;max-width:500px;max-height:500px'])
        <h1 class="h1 text-center text-white ct-font light-font-weight capitalize">NYMPHAE</h1>
    </div>
    <div class="col-12 py-5">
        <hr>
        @guest
            <a href="/login" class="btn btn-primary btn-block ct-font capitalize light-font-weight py-2">Connexion</a>
            <br>
            <a href="/register" class="btn btn-primary btn-block ct-font capitalize light-font-weight py-2">Inscription</a>
        @endguest
        @auth
            @if(Auth::user()->is_appart_user === 1 || Auth::user()->is_admin === 1)
                <a href="/course"
                   class="btn btn-primary btn-block ct-font capitalize light-font-weight py-2">Courses</a>
            @endif
            <br>
            <br>
            <br>
            <button class="btn btn-custom-cancel btn-block ct-font capitalize light-font-weight py-2">Deconnexion
            </button>
        @endauth
    </div>
@endsection
