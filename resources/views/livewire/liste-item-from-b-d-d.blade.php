<div class="col-12">
    <br>
    {{-- To attain knowledge, add things every day; To attain wisdom, subtract things every day --}}
    <table class="table table-hover table-striped rounded border-light text-center">
        <thead>
        <tr>
            @foreach($colTypes as $col => $type)
                <th>{{$col}}</th>
            @endforeach
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $d)
            <tr>
                @foreach($colTypes as $col => $type)
                    <td>{{$d->$col}}</td>
                @endforeach
                <td>
                    <button class="btn btn-outline-info"><i class="fas fa-pen"></i></button>
                    <form action="{{route('produit--delete')}}" method="GET" class="d-inline">
                        <input type="hidden" name="id" value="{{$d->id}}">
                        <button type="submit" class="btn btn-outline-danger"><i
                                class="fas fa-trash"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
