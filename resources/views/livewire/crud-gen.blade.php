<div class="col-12">
    <style>
        .hide_content{
            display: none;
        }

        .eye-slash {
            display: none;
        }
    </style>
    {{-- The whole world belongs to you --}}
    <div class="card text-white bg-dark">
        <div class="card-header">
            <div class="navbar navbar-light">
                <div class="navbar-brand">
                    <span class="h2 text-white">Crud Generator : {{$class}}</span>
                </div>
                <div class="nav nav-pills">
                    <button class="btn btn-outline-light btn-level" data-bs-toggle="modal" data-bs-target="#ModalAdd{{$class}}">
                        <span class="h5">
                            <i class="fal fa-plus"></i>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="card-body">
           <table class="table table-hover table-striped table-dark text-center">
               <thead>
                    <tr>
                        @foreach($colsOfData as $col => $type)
                            <th>{{$col}}</th>
                        @endforeach
                        <th>Action</th>
                    </tr>
               </thead>
               <tbody>
                    @foreach($data as $d)
                        <tr>
                            @foreach($colsOfData as $col => $type)
                                <td>{{$d->$col}}</td>
                            @endforeach
                            <td>
                                <span data-toggle="tooltip" data-placement="top" title="Modification de cet élément">
                                    <button class="btn btn-outline-info btn-level" data-toggle="modal" data-bs-target="#ModalEditElement{{$class}}{{$d->id}}">
                                        <i class="fad fa-edit"></i>
                                    </button>
                                </span>
                                <span data-toggle="tooltip" data-placement="top" title="Suppression de cet élément">
                                    <button class="btn btn-outline-danger btn-level" onclick="if(confirm('Etes vous sûr de supprimer cette élement ?')){$('#formDelete{{$class}}{{$d->id}}').submit();}">
                                        <i class="fad fa-trash"></i>
                                    </button>
                                    <form action="{{Route($class . '--delete')}}" method="get" id="formDelete{{$class}}{{$d->id}}" style="display: none;">
                                        @csrf
                                        <input name="id" type="hidden" value="{{$d->id}}">
                                    </form>
                                </span>
                            </tr>
                    @endforeach
               </tbody>
           </table>
        </div>
    </div>
    <br>
    @push('modal')
    <!--MODAL AJOUT-->
        <!-- Modal -->
        <div class="modal fade" id="ModalAdd{{$class}}" tabindex="-1" aria-labelledby="ModalNewElement{{$class}}" aria-hidden="true">
            <div class="modal-dialog modal-lg bg-dark">
                <div class="modal-content bg-dark text-white">
                    <div class="modal-header text-white">
                        <h5 class="modal-title" id="exampleModalLabel">Ajout pour {{$class}}</h5>
                        <button type="button" class="btn-close text-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body text-white">
                        <div class="navbar navbar-light">
                            <div class="navbar-brand">
                                <span class="h6 text-white">
                                    Nouvelle élément
                                </span>
                            </div>
                            <div class="nav nav-pills">
                                <button class="btn btn-outline-light btn-level toggle_input_view">
                                    <i class="fal fa-eye eye"></i>
                                    <i class="fal fa-eye-slash eye-slash"></i>
                                </button>
                            </div>
                        </div>
                        <form action="{{Route($class . '--create')}}" method="get" id="newSave{{$class}}">
                            @csrf
                            <div class="form-group row">
                                @foreach($colsOfData as $col => $type)
                                    @if($col === 'id')
                                        <div class="col-6 hide_content">
                                            <label for="new{{$class}}{{$col}}">{{$col}}</label>
                                            <input type="number" class="form-control-plaintext bg-secondary rounded px-1 text-white" id="new{{$class}}{{$col}}" name="{{$col}}">
                                        </div>
                                    @elseif($col === 'created_at' || $col === 'updated_at')
                                        <div class="col-3 hide_content">
                                            <label for="new{{$class}}{{$col}}">{{$col}} date</label>
                                            <input type="date" class="form-control-plaintext bg-secondary rounded px-1 text-white" id="new{{$class}}{{$col}}" name="{{$col}}_date">
                                        </div>

                                        <div class="col-3 hide_content">
                                            <label for="new{{$class}}{{$col}}">{{$col}} time</label>
                                            <input type="time" class="form-control-plaintext bg-secondary rounded px-1 text-white" id="new{{$class}}{{$col}}" name="{{$col}}_time">
                                        </div>
                                    @else
                                        @if($type === 'datetime')
                                            <div class="col-3 hide_content">
                                                <label for="new{{$class}}{{$col}}">{{$col}} date</label>
                                                <input type="date" class="form-control-plaintext bg-secondary rounded px-1 text-white" id="new{{$class}}{{$col}}" name="{{$col}}_date">
                                            </div>

                                            <div class="col-3 hide_content">
                                                <label for="new{{$class}}{{$col}}">{{$col}} time</label>
                                                <input type="time" class="form-control-plaintext bg-secondary rounded px-1 text-white" id="new{{$class}}{{$col}}" name="{{$col}}_time">
                                            </div>
                                        @else
                                            <div class="col-6">
                                                <label for="new{{$class}}{{$col}}">{{$col}}</label>
                                                <input
                                                    @if($type === "bigint" || $type === "integer")
                                                    type="number"
                                                    @elseif($type === "string")
                                                    type="text"
                                                    @endif
                                                    class="form-control-plaintext bg-secondary rounded px-1 text-white" id="new{{$class}}{{$col}}" name="{{$col}}">
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer text-white">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal"><i class="fad fa-portal-exit"></i></button>
                        <button type="button" class="btn btn-primary" onclick="$('#newSave{{$class}}').submit()"><i class="fad fa-save"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <!--MODAL EDITION-->
        @foreach($data as $d)
            <div class="modal fade" id="ModalEditElement{{$class}}{{$d->id}}" tabindex="-1" aria-labelledby="ModalEditElement{{$class}}{{$d->id}}" aria-hidden="true">
                <div class="modal-dialog modal-lg bg-dark">
                    <div class="modal-content bg-dark text-white">
                        <div class="modal-header text-white">
                            <h5 class="modal-title" id="exampleModalLabel">{{$class}} : Edition de l'élément : {{$d->id}}</h5>
                            <button type="button" class="btn-close text-white" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body text-white">
                            <div class="navbar navbar-light">
                                <div class="navbar-brand">
                                <span class="h6 text-white">
                                    Modification
                                </span>
                                </div>
                                <div class="nav nav-pills">
                                    <button class="btn btn-outline-light btn-level toggle_input_view">
                                        <i class="fal fa-eye eye"></i>
                                        <i class="fal fa-eye-slash eye-slash"></i>
                                    </button>
                                </div>
                            </div>
                            <form action="{{Route($class . '--update')}}" method="get" id="edit{{$class}}{{$d->id}}">
                                @csrf
                                <div class="form-group row">
                                    @foreach($colsOfData as $col => $type)
                                        <input type="hidden" id="old_id" name="old_id" value="{{$d->id}}">
                                        @if($col === 'id')
                                            <div class="col-6 hide_content">
                                                <label for="edit{{$class}}{{$d->id}}{{$col}}">{{$col}}</label>
                                                <input type="number" class="form-control-plaintext bg-secondary rounded px-1 text-white" id="edit{{$class}}{{$d->id}}{{$col}}" name="{{$col}}" value="{{$d->$col}}">
                                            </div>
                                        @elseif($col === 'created_at' || $col === 'updated_at')
                                            <div class="col-3 hide_content">
                                                <label for="edit{{$class}}{{$d->id}}{{$col}}">{{$col}} date</label>
                                                <input type="date" class="form-control-plaintext bg-secondary rounded px-1 text-white" id="edit{{$class}}{{$d->id}}{{$col}}" name="{{$col}}_date" value="<?php $datetime = explode(' ', $d->$col); echo $datetime[0]; ?>">
                                            </div>

                                            <div class="col-3 hide_content">
                                                <label for="edit{{$class}}{{$d->id}}{{$col}}">{{$col}} time</label>
                                                <input type="time" class="form-control-plaintext bg-secondary rounded px-1 text-white" id="edit{{$class}}{{$d->id}}{{$col}}" name="{{$col}}_time" value="<?php echo substr($datetime[1],0 ,-3);?>">
                                            </div>
                                        @else
                                            @if($type === 'datetime')
                                                <div class="col-3 hide_content">
                                                    <label for="edit{{$class}}{{$d->id}}{{$col}}">{{$col}} date</label>
                                                    <input type="date" class="form-control-plaintext bg-secondary rounded px-1 text-white" id="edit{{$class}}{{$d->id}}{{$col}}" name="{{$col}}_date" value="<?php $datetime = explode(' ', $d->$col); echo $datetime[0]; ?>">
                                                </div>

                                                <div class="col-3 hide_content">
                                                    <label for="edit{{$class}}{{$d->id}}{{$col}}">{{$col}} time</label>
                                                    <input type="time" class="form-control-plaintext bg-secondary rounded px-1 text-white" id="edit{{$class}}{{$d->id}}{{$col}}" name="{{$col}}_time" value="<?php echo substr($datetime[1],0 ,-3);?>">
                                                </div>
                                            @else
                                                <div class="col-6">
                                                    <label for="edit{{$class}}{{$d->id}}{{$col}}">{{$col}}</label>
                                                    <input
                                                        @if($type === "bigint" || $type === "integer")
                                                        type="number"
                                                        @elseif($type === "string")
                                                        type="text"
                                                        @endif
                                                        class="form-control-plaintext bg-secondary rounded px-1 text-white" id="edit{{$class}}{{$d->id}}{{$col}}" name="{{$col}}" value="{{$d->$col}}">
                                                </div>
                                            @endif
                                        @endif
                                    @endforeach
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer text-white">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal"><i class="fad fa-portal-exit"></i></button>
                            <button type="button" class="btn btn-primary" onclick="$('#edit{{$class}}{{$d->id}}').submit()"><i class="fad fa-save"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endpush
    @push('script')
        @if($putScript === "true")
            <script>
                window.addEventListener('load', function () {
                    $('.toggle_input_view').on('click', function() {
                        $('.hide_content').toggle();
                        $('.eye').toggle();
                        $('.eye-slash').toggle();
                    })
                })
            </script>
        @endif
    @endpush
</div>
