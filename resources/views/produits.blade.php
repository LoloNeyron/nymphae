@extends('layouts.web')

@section('title', 'Projet')

@section('content')
    <div class="wrapper">
        {{--content --}}
        <div id="content" class="col-12">
            <div class="row p-4">
                <div class="col-12">
                    <div class="row pb-3">
                        <div class="col-6">
                            <button class="btn btn-primary" data-toggle="collapse" href="#formCreationProduit" role="button"
                                    aria-expanded="false" aria-controls="formCreationProduit">
                                Créer un nouveau produit
                            </button>
                        </div>
                        <div class="col-6">
                            <p class="nav justify-content-end">
                                <button type="button" class="btn btn-info sidebarCollapse">
                                    <i class="fas fa-search"></i>
                                    <span>Listes des produit</span>
                                </button>
                            </p>
                        </div>
                    </div>
                    <div class="collapse" id="formCreationProduit">
                        <div class="card card-body">
                            <h4>Nouveau projet : <span id="displayProduitName"></span></h4>
                            <form action="{{Route('produit--create')}}" method="GET">
                                @csrf
                                <small>ID : {{$lastId}}</small>
                                <br>
                                <div class="row">
                                    <div class="col-12">
                                        <label for="nameProduit">Nom</label>
                                        <input id="nameProduit" required type="text" name="name" class="form-control"
                                               placeholder="Name">
                                        <br>
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label for="prixProduit">Prix</label>
                                        <input id="prixProduit" required type="number" step="0.001" name="prix" class="form-control">
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label for="uniteProduit">unité</label>
                                        <input id="uniteProduit" required type="number" step="1" name="unite" class="form-control" value="1">
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <label for="tvaProduit">TVA en %</label>
                                        <input id="tvaProduit" required type="number" step="0.01" name="tva" class="form-control" value="0">
                                    </div>
                                    <div class="col-12">
                                        <br>
                                        <label for="descriptionProduit">Description</label>
                                        <textarea  class="form-control" name="description" id="descriptionProduit" rows="3"></textarea>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-success btn-block">Enregistrer</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--end content --}}

        {{--sideSearch --}}
        <div class="nav p-3 bg-white shadow" id="sideSearchTab">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-8">
                        <h1>Liste des produits :</h1>
                    </div>
                    <div class="col-4">
                        <p class="nav justify-content-end">
                            <button type="button" class="btn btn-info sidebarCollapse">
                                <i class="fas fa-times"></i>
                            </button>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <livewire:liste-item-from-b-d-d />
                </div>
            </div>
        </div>
        {{--end sideSearch --}}
    </div>
@endsection
