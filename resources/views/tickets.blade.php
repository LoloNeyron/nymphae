@extends('layouts.web')

@push('header')
    <link rel="stylesheet" href="{{url('css/jqueryContextMenu.min.css')}}">
@endpush

@section('title', 'Tickets')

@section('content')
    <div class="container-fluid p-4" id="headerTickets">
        <div class="row">
            <div class="col-12">
                <button class="btn btn-dark btn-level" data-toggle="collapse" href="#FormNewTicket" role="button"
                        aria-expanded="false" aria-controls="FormNewTicket">Nouveau ticket &nbsp;<h5 class="d-inline"><i
                            class="fal fa-ticket"></i></h5></button>
                <div class="row">
                    <div class="collapse col-12" id="FormNewTicket">
                        <div class="card card-body">
                            <h3>Nouveau Ticket&nbsp;<i class="fal fa-ticket"></i></h3>
                            <hr>
                            <form action="{{Route('Ticket--create')}}" method="POST" id="TicketForm">
                                @csrf
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <input type="hidden" id="TicketsSupp" name="TicketsSupp">
                                <div class="form-group">
                                    <label for="TicketTitle">Titre du ticket</label>
                                    <input type="text" class="form-control" name="TicketTitle" id="TicketTitle"
                                           placeholder="">
                                </div>
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="ticketState">L'état du ticket</label>
                                        <select required name="ticketState" class="form-control" id="ticketState">
                                            <option selected>Choisir un état :</option>
                                            <option class="bg-danger" value="Urgent">Urgent</option>
                                            <option class="bg-warning" value="Warning">ça presse</option>
                                            <option class="bg-success" value="todo">A faire</option>
                                            <option class="bg-info" value="info">Besoin d'information</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="project">Le projet</label>
                                        <select required name="idProject" class="form-control" id="project">
                                            <option selected>Choisir un projet :</option>
                                            @foreach($projects as $p)
                                                <option value="{{$p->id}}">{{$p->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="AjoutSuppTicket" class="col-12 sortable">
                                    </div>
                                    <div class="col-12">
                                        <button type="button" class="btn btn-outline-info pr-2 mr-2 ajoutSuppText"><i
                                                class="fad fa-plus"></i>&nbsp;Ajout de text
                                        </button>
                                        <button type="button" class="btn btn-outline-info pr-2 mr-2 ajoutSuppBtn"><i
                                                class="fad fa-plus"></i>&nbsp;Ajout d'un bouton
                                        </button>
                                    </div>
                                </div>
                                <div class="row pt-3">
                                    <div class="col-12">
                                        <button class="btn btn-outline-success btn-block" type="button" onclick="submitFromTicket()">Enregistrer&nbsp;<i class="fad fa-save"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            @foreach($tickets as $ticket)
                <div class="card col-12 mb-3
                     @if($ticket->state === "Urgent")
                        urgentBorder
                    @elseif($ticket->state === "Warning")
                        warningBorder
                    @elseif($ticket->state === "todo")
                        todoBorder
                    @elseif($ticket->state === "info")
                        infoBorder
                    @endif
                 animate context-menu-one" numTicket="{{$ticket->id}}">
                    <div class="card-body">
                        <h5 class="card-title d-inline">{{$ticket->title}} | <span class="project-bubble">@foreach($projects as $p) @if($ticket->idproject === $p->id) {{$p->name}} @endif @endforeach</span></h5>
                        @if($ticket->state === "Urgent")
                            <span class="urgentTicket shadow-lg"><span class="textState">Urgent !</span></span>
                        @elseif($ticket->state === "Warning")
                            <span class="warningTicket shadow-lg"><span class="textState">ça presse !</span></span>
                        @elseif($ticket->state === "todo")
                            <span class="todoTicket shadow-lg"><span class="textState">A faire !</span></span>
                        @elseif($ticket->state === "info")
                            <span class="infoTicket shadow-lg"><span class="textState">Besoin d'informations !</span></span>
                        @endif
                        <hr>
                       @foreach($torders as $to)
                           @if($to->id_ticket === $ticket->id)
                                @if($to->id_tbtn !== NULL)
                                    @foreach($tbtns as $tb)
                                        @if($tb->id === $to->id_tbtn)
                                            <a href="{{$tb->link}}" class="{{$tb->class}}">{{$tb->title}}</a>
                                        @endif
                                    @endforeach
                                @endif
                                @if($to->id_tcontent !== NULL)
                                    @foreach($tcontents as $tc)
                                            @if($tc->id === $to->id_tcontent)
                                                <p class="{{$tc->class}}">{{$tc->content}}</p>
                                            @endif
                                    @endforeach
                                @endif
                            @endif
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>

{{--        <div class="row">--}}

{{--            <div class="card col-12 mb-3 todoBorder animate context-menu-one" numTicket="2">--}}
{{--                <div class="card-body">--}}
{{--                    <h5 class="card-title d-inline">Title | <span class="project-bubble">Pierre Soral</span></h5>--}}
{{--                    <span class="todoTicket shadow-lg"><span class="textState">A faire !</span></span>--}}
{{--                    <hr>--}}
{{--                    <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>--}}
{{--                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the--}}
{{--                        card's content.</p>--}}
{{--                    <a href="#" class="card-link">Card link</a>--}}
{{--                    <a href="#" class="card-link">Another link</a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="card col-12 mb-3 warningBorder animate context-menu-one" numTicket="3">--}}
{{--                <div class="card-body">--}}
{{--                    <h5 class="card-title d-inline">Title | <span class="project-bubble">Pierre Soral</span></h5>--}}
{{--                    <span class="warningTicket shadow-lg"><span class="textState">ça presse !</span></span>--}}
{{--                    <hr>--}}
{{--                    <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>--}}
{{--                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the--}}
{{--                        card's content.</p>--}}
{{--                    <a href="#" class="card-link">Card link</a>--}}
{{--                    <a href="#" class="card-link">Another link</a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="card col-12 mb-3 infoBorder animate context-menu-one" numTicket="4">--}}
{{--                <div class="card-body">--}}
{{--                    <h5 class="card-title d-inline">Title | <span class="project-bubble">Pierre Soral</span></h5>--}}
{{--                    <span class="infoTicket shadow-lg"><span class="textState">Besoin d'informations !</span></span>--}}
{{--                    <hr>--}}
{{--                    <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>--}}
{{--                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the--}}
{{--                        card's content.</p>--}}
{{--                    <a href="#" class="card-link">Card link</a>--}}
{{--                    <a href="#" class="card-link">Another link</a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
@endsection

@push('script')

    <script src="{{url('js/jqueryContextMenu.min.js')}}"></script>
    <script src="https://unpkg.com/sortablejs-make/Sortable.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>
    <script>
        var totalBtn = 0;
        var totalText = 0;
        $('.ajoutSuppText').on('click', function () {
            totalText++;
            $('#AjoutSuppTicket').append(fillNewSlotTicketSupp('text'));
            reorderEverything();
        })

        $('.ajoutSuppBtn').on('click', function () {
            totalBtn++;
            $('#AjoutSuppTicket').append(fillNewSlotTicketSupp('btn'));
            reorderEverything();
        })

        function submitFromTicket(){
            reorderEverything();
            $('#TicketForm').submit();
        }


        function fillNewSlotTicketSupp(type){
            var rand = makeid(6);
            console.log(rand);
            if(type === "btn"){
                return '<fieldset class="mb-2 animate suppElement context-menu-edit-present-ticket" ticketRandId="'+rand+'" ticketsupporder="'+ (totalBtn + totalText) +'" ticketsupptype="btn" data-id="'+ (totalBtn + totalText) +'">' +
                    '<legend>Nouveaux bouton : <span class="nameFromNewBtn"></span><i class="fas fa-grip-lines handle"></i></legend>' +
                    '<div class="row">' +
                    '<div class="col-6">' +
                    '<label for="titleBtn'+rand+'" class="form-label">Title</label>' +
                    '<input type="text" class="form-control inputWatcher" id="titleBtn'+rand+'">' +
                    '</div> ' +
                    '<div class="col-6">' +
                    '<label for="classBtn'+rand+'" class="form-label">Class</label>' +
                    '<input type="text" class="form-control inputWatcher" id="classBtn'+rand+'">' +
                    '</div>' +
                    '<div class="col-12">' +
                    '<label for="linkBtn'+rand+'" class="form-label">Link</label>' +
                    '<input type="text" class="form-control inputWatcher" id="linkBtn'+rand+'">' +
                    '</div>' +
                    '</div>' +
                    '</fieldset>';
            }else{
                return '<fieldset class="mb-2 animate suppElement context-menu-edit-present-ticket" ticketRandId="'+rand+'" ticketsupporder="'+ (totalBtn + totalText) +'" ticketsupptype="text" data-id="'+ (totalBtn + totalText) +'">' +
                    '<legend>Nouveaux text : <span class="nameFromNewText"></span><i class="fas fa-grip-lines handle"></i></legend>' +
                    '<div class="row">' +
                    '<div class="col-12">' +
                    '<label for="classText'+rand+'" class="form-label">Class</label>' +
                    '<input type="text" class="form-control inputWatcher" id="classText'+rand+'">' +
                    '</div>' +
                    '<div class="col-12">' +
                    '<label for="contentText'+rand+'" class="form-label">Contenu</label>' +
                    '<textarea type="text" class="form-control inputWatcher" id="contentText'+rand+'"></textarea>' +
                    '</div>' +
                    '</div>' +
                    '</fieldset>';

            }

        }

        // Arrays of "data-id"
        $('#get-order').click(function() {
            var sort1 = $('#AjoutSuppTicket').sortable('toArray');
            console.log(sort1);
        });

        // $('').keyup(function (){
        //     console.log('Keyup');
        //     reorderEverything();
        // });



        function makeid(length) {
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        }

        function reorderEverything(){
            var order = 0;
            $('#AjoutSuppTicket fieldset').each(function(index){
                order++;
                if($(this).attr('ticketsupptype') === "text"){
                    $(this).attr('ticketsupporder', order);
                    $(this).attr('data-id', order);
                }else if($(this).attr('ticketsupptype') === "btn"){
                    $(this).attr('ticketsupporder', order);
                    $(this).attr('data-id', order);
                }
            })
            MajInputForSuppFields();
        }

        function MajInputForSuppFields(){
            var jsonTabInput = [];
            var id= "";
            $('#AjoutSuppTicket fieldset').each(function(index){
                if($(this).attr('ticketsupptype') === "text"){
                    id = $(this).attr('ticketRandId');
                    jsonTabInput.push({
                        "type": "text",
                        "class": $('#classText' + id).val(),
                        "content": $('#contentText' + id).val()
                    });
                }else if($(this).attr('ticketsupptype') === "btn"){
                    id = $(this).attr('ticketRandId');
                    jsonTabInput.push({
                        "type": "btn",
                        "class": $('#classBtn' + id).val(),
                        "link": $('#linkBtn' + id).val(),
                        "title": $('#titleBtn' + id).val()
                    });
                }
            })
            $('#TicketsSupp').val(JSON.stringify(jsonTabInput));
        }

        $(function () {
            // With options:
            $('.sortable').sortable({
                handle: '.handle',
                group: 'list',
                animation: 150,
                ghostClass: 'ghost',
                onSort:reorderEverything,
            })
            $.contextMenu({
                selector: '.context-menu-one',
                callback: function (key, options) {
                    var m = "clicked: " + key;
                    window.console && console.log(m, $(this).attr('numticket'));
                    //|| alert(m)
                },
                items: {
                    "edit": {name: "Modifier", icon: "fad fa-edit"},
                    "cut": {name: "Cut", icon: "fad fa-cut"},
                    copy: {name: "Copy", icon: "fad fa-copy"},
                    "paste": {name: "Paste", icon: "fad fa-paste"},
                    "delete": {name: "Delete", icon: "fad fa-trash"},
                    "sep1": "---------",
                    "quit": {name: "Quit", icon: "fad fa-sign-out"},
                }
            });

            $('.context-menu-one').on('click', function (e) {
                console.log('clicked', this, $(this).attr('numTicket'));
            })

            $.contextMenu({
                selector: '.context-menu-edit-present-ticket',
                callback: function (key, options) {
                    var m = "clicked: " + key;
                    window.console && console.log(m, $(this).attr('numticket'));
                    //|| alert(m)
                },
                items: {
                    "delete": {
                        name: "Delete",
                        icon: "fad fa-trash",
                        callback: function(key, opt){
                            if($(this).attr('ticketsupptype') === "text"){
                                totalText--;
                            }else{
                                totalBtn--;
                            }
                            $(this).remove();
                            reorderEverything();
                        }
                    }
                }
            });
        });
    </script>
@endpush
