@extends('layouts.webMobile')

@section('title', 'Course')

@section('content')

    <div class="col-12 text-center text-white">
        @include('components.application-logo', ['attributes' => 'style=display:block;margin:auto;width:25vw;color:rgba(75,60,98);fill:currentColor;height:25vw;max-width:300px;max-height:300px'])
        <h5 class="h5 text-center text-white ct-font light-font-weight capitalize">NYMPHAE</h5>
    </div>
    <div class="col-12 text-center text-white">
        <hr>
        <h1 class="light-font-weight py-3">{{$lc->name}}</h1>
        <h3 class="h6 light-font-weight py-1"><small>{{Jenssegers\Date\Date::parse($lc->created_at)->format('\Le l j F Y à H:i')}}</small></h3>
        <p class="alert alert-dark">{{$lc->description}}</p>
        <hr>
    </div>
    <div class="col-12">
        <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#ModalNewItem">Ajouter un produit</button>
        <hr>
        <table class="table table-hover table-dark table-striped">
            <thead class="text-center">
            <tr>
                <th style="max-width: 20px">Action</th>
                <th>Produit</th>
            </tr>
            </thead>
            <tbody class="text-center">
            @foreach($product as $p)
                @if($p->isChecked == 0)
                    <tr class="checkable" id="{{$p->id}}" isChecked="{{$p->isChecked}}">
                        <td id="{{$p->id}}_action" style="max-width: 20px"><div id="{{$p->id}}_square" class="square"></div></td>
                        <td id="{{$p->id}}_name">{{$p->name}}</td>
                    </tr>
                @else
                    <tr class="checkable" id="{{$p->id}}" isChecked="{{$p->isChecked}}">
                        <td id="{{$p->id}}_action" style="max-width: 20px"><div id="{{$p->id}}_square" class="square-checked"></div></td>
                        <td id="{{$p->id}}_name" class="text-cut">{{$p->name}}</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@push('modal')
    <div class="modal fade" id="ModalNewItem" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ajout d'un produit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{Route('newItem')}}" method="GET">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                        <input type="hidden" name="liste_id" value="{{$lc->id}}">
                        <div class="form-group">
                            <label for="name">Nom</label>
                            <input type="text" name="name" class="form-control" id="name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endpush

@push('script')
    <script>
        $('.checkable').on('click', function() {
            var id = $(this).attr('id');
            console.log($(this).attr('ischecked'));
            if($(this).attr('ischecked') == 0){
                console.log('test');
                $('#' + id + '_square').removeClass('square').addClass('square-checked');
                $('#' + id + '_name').addClass('text-cut');
                $(this).attr('ischecked', 1);
                CheckAjax(id);
            }else{
                console.log('test here');
                $('#' + id + '_square').removeClass('square-checked').addClass('square');
                $('#' + id + '_name').removeClass('text-cut');
                $(this).attr('ischecked', 0);
                CheckAjax(id);
            }
        })

        function CheckAjax(id){
            $.ajax({
                method: "get",
                url: "{{Route('checkaproduct')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                }
            }).done(function( msg ) {
                if(msg.error == 0){
                    //alert(msg.message);
                }else{
                    //alert(msg.message);
                }
            });
        }
    </script>
@endpush
