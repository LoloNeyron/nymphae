<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('ico/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('ico/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('ico/favicon-16x16.png')}}">
    <link rel="manifest" href="{{url('ico/site.webmanifest')}}">
    <link rel="mask-icon" href="{{url('ico/safari-pinned-tab.svg')}}" color="#000000">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="theme-color" content="#ffffff">


    <title>@yield('title') | Dashboard</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
          integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    {{--    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>--}}

    <link rel="stylesheet" href="{{url('css/all.css')}}">
    <link rel="stylesheet" href="{{url('css/iziToast.min.css')}}">
    <link rel="stylesheet" href="{{url('css/style.css')}}">
    @stack('header')
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light shadow z-100">
    <div class="container-fluid">
        @include('components.application-logo', ['attributes' => 'style=width:3rem;color:rgba(107,114,128);fill:currentColor;height:3em;'])
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav mr-auto">
                @if (Route::has('login'))
                    @auth
                        <a href="{{ url('/') }}" class="nav-link">Web</a>
                        <a href="{{ url('/dashboard') }}" class="nav-link">Dashboard</a>
                    @else
                        <a href="{{ route('login') }}" class="nav-link">Log in</a>
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="nav-link">Register</a>
                        @endif
                    @endauth
                @endif
            </div>
            <div class="ml-auto">
                <div class="dropdown">
                    <button class="round" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user-alt"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                        @if (Route::has('login'))
                            @auth
                                <form action="{{url('/logout')}}" method="POST">
                                    @csrf
                                    <button class="dropdown-item btn btn-dark">Logout</button>
                                </form>
                            @endauth
                        @endif
                    </div>
                </div>
            </div>
        </div>
</nav>
<div class="container-fluid" style="min-height: 100vh">
    <div class="row" style="min-height: 100vh">
        <div class="col-1 shadow-lg bg-light z-50" style="min-height: 100vh">
            <ul class="nav flex-column pt-3">
                @if(Auth::user()->is_devis_user === 1 || Auth::user()->is_admin === 1)
                    <li class="pt-1">
                        <a class="btn @if(Route::is('projetAdmin')) btn-outline-dark @else btn-dark @endif btn-block"
                           href="{{route('projetAdmin')}}">
                            Projets
                        </a>
                    </li>
                @endif
                @if(Auth::user()->is_admin === 1)
                    <li class="pt-1">
                        <a class="btn @if(Route::is('chatIndex')) btn-outline-dark @else btn-dark @endif btn-block"
                           href="{{route('chatIndex')}}">
                            Chat
                        </a>
                    </li>
                @endif
                @if(Auth::user()->is_devis_user === 1 || Auth::user()->is_admin === 1)
                    <li class="pt-1">
                        <a class="btn @if(Route::is('indexClient')) btn-outline-dark @else btn-dark @endif btn-block"
                           href="{{route('indexClient')}}">
                            Clients
                        </a>
                    </li>
                @endif
                @if(Auth::user()->is_admin === 1)
                    <li class="pt-1">
                        <a class="btn @if(Route::is('tickets')) btn-outline-dark @else btn-dark @endif btn-block"
                           href="{{route('tickets')}}">
                            Tickets
                        </a>
                    </li>
                @endif
                @if(Auth::user()->is_admin === 1)
                    <li class="pt-1">
                        <a class="btn btn-dark btn-block" href="">
                            Tecalist
                        </a>
                    </li>
                @endif
                @if(Auth::user()->is_devis_user === 1 || Auth::user()->is_admin === 1)
                    <li class="pt-1">
                        <a class="btn @if(Route::is('produits')) btn-outline-dark @else btn-dark @endif btn-block"
                           href="{{route('produits')}}">
                            Produits
                        </a>
                    </li>
                @endif
                @if(Auth::user()->is_admin === 1)
                    <li class="pt-1">
                        <a class="btn btn-dark btn-block" href="">
                            Tools
                        </a>
                    </li>
                @endif
                @if(Auth::user()->is_appart_user === 1 || Auth::user()->is_admin === 1)
                    <li class="pt-1">
                        <a class="btn @if(Route::is('course')) btn-outline-dark @else btn-dark @endif btn-block" href="/course">
                            Course
                        </a>
                    </li>
                @endif
            </ul>
        </div>
        <div class="col-11 bg-light z-30">
            @yield('content')
        </div>
    </div>
</div>
@stack('modal')
<script src="{{url('js/all.js')}}"></script>
<script src="{{url('js/iziToast.min.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('.sidebarCollapse').on('click', function () {
            $('#sideSearchTab').toggleClass('active');
        });
        $('#content').on('click', function () {

        })
        iziToast.settings({
            timeout: 4500,
            resetOnHover: true,
            position: 'topRight',
            transitionIn: 'flipInX',
            transitionOut: 'flipOutX',
            theme: 'light',
            progressBarColor: 'rgb(128, 0, 128)'
        });
        @if(session('message'))
        @if(session('status') === 'danger')
        iziToast.success({
            color: 'red',
            title: '{{session('title')}}',
            icon: 'fa fa-exclamation',
            message: '{{session('message')}}',
        });
        @elseif((session('status') === 'success'))
        iziToast.success({
            color: 'green',
            title: '{{session('title')}}',
            icon: 'fas fa-check',
            message: '{{session('message')}}',
        });
        @elseif((session('status') === 'info'))
        iziToast.success({
            color: 'blue',
            title: '{{session('title')}}',
            icon: 'fas fa-info',
            message: '{{session('message')}}',
        });
        @elseif((session('status') === 'classic'))
        iziToast.show({
            theme: 'dark',
            title: '{{session('title')}}',
            icon: 'fad fa-caret-right',
            message: '{{session('message')}}',
        });
        @endif
        @endif
    });
    var themeColor = "light";
    // function test(){
    //     if(themeColor === "light"){
    //         //light to dark
    //         $(".bg-light").each(function (){
    //             $(this).removeClass('bg-light').addClass('bg-dark')
    //         })
    //         $(".btn-dark").each(function (){
    //             $(this).removeClass('btn-dark').addClass('btn-light')
    //         })
    //         $(".table-light").each(function (){
    //             $(this).removeClass('table-light').addClass('table-dark')
    //         })
    //         themeColor = "dark";
    //     }else{
    //         //dark to light
    //         $(".bg-dark").each(function (){
    //             $(this).removeClass('bg-dark').addClass('bg-light')
    //         })
    //         $(".btn-light").each(function (){
    //             $(this).removeClass('btn-light').addClass('btn-dark')
    //         })
    //         $(".table-dark").each(function (){
    //             $(this).removeClass('table-dark').addClass('table-light')
    //         })
    //         themeColor = "light";
    //     }
    // }
</script>
@stack('script')
</body>
</html>
