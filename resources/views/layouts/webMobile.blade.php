<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('ico/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('ico/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('ico/favicon-16x16.png')}}">
    <link rel="manifest" href="{{url('manifest.webmanifest')}}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/all.css')}}">
    <link rel="stylesheet" href="{{url('css/iziToast.min.css')}}">
    <link rel="stylesheet" href="{{url('css/style.css')}}">
</head>
<body style="min-height:100vh; background-color: #1a202c">
<div class="container-fluid ct-font">
    <div class="row">
        @yield('content')
    </div>
</div>
@stack('modal')
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="{{url('js/all.js')}}"></script>
<script src="{{url('js/iziToast.min.js')}}"></script>
<script src="{{url('js/generate-sw.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('.sidebarCollapse').on('click', function () {
            $('#sideSearchTab').toggleClass('active');
        });
        $('#content').on('click', function () {

        })
        iziToast.settings({
            timeout: 4500,
            resetOnHover: true,
            position: 'topRight',
            transitionIn: 'flipInX',
            transitionOut: 'flipOutX',
            theme: 'light',
            progressBarColor: 'rgb(128, 0, 128)'
        });
        @if(session('message'))
        @if(session('status') === 'danger')
        iziToast.success({
            color: 'red',
            title: '{{session('title')}}',
            icon: 'fa fa-exclamation',
            message: '{{session('message')}}',
        });
        @elseif((session('status') === 'success'))
        iziToast.success({
            color: 'green',
            title: '{{session('title')}}',
            icon: 'fas fa-check',
            message: '{{session('message')}}',
        });
        @elseif((session('status') === 'info'))
        iziToast.success({
            color: 'blue',
            title: '{{session('title')}}',
            icon: 'fas fa-info',
            message: '{{session('message')}}',
        });
        @elseif((session('status') === 'classic'))
        iziToast.show({
            theme: 'dark',
            title: '{{session('title')}}',
            icon: 'fad fa-caret-right',
            message: '{{session('message')}}',
        });
        @endif
        @endif
    });
    var themeColor = "light";
    // function test(){
    //     if(themeColor === "light"){
    //         //light to dark
    //         $(".bg-light").each(function (){
    //             $(this).removeClass('bg-light').addClass('bg-dark')
    //         })
    //         $(".btn-dark").each(function (){
    //             $(this).removeClass('btn-dark').addClass('btn-light')
    //         })
    //         $(".table-light").each(function (){
    //             $(this).removeClass('table-light').addClass('table-dark')
    //         })
    //         themeColor = "dark";
    //     }else{
    //         //dark to light
    //         $(".bg-dark").each(function (){
    //             $(this).removeClass('bg-dark').addClass('bg-light')
    //         })
    //         $(".btn-light").each(function (){
    //             $(this).removeClass('btn-light').addClass('btn-dark')
    //         })
    //         $(".table-dark").each(function (){
    //             $(this).removeClass('table-dark').addClass('table-light')
    //         })
    //         themeColor = "light";
    //     }
    // }
</script>
@stack('script')
</body>
</html>
