@extends('layouts.web')

@section('title', 'dashboard')

@section('content')

    <div class="row p-4">
        <div class="col-12">
           <div class="card">
               <div class="card-header">
                   <h3>Dashboard</h3>
               </div>
               <div class="card-body">
                   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto culpa deserunt dignissimos dolores eos esse illo illum ipsa ipsum itaque labore, libero magni neque nobis obcaecati, perferendis, reprehenderit repudiandae voluptatum?</p>
               </div>
               <div class="card-footer">
                   <div class="d-inline ml-auto">
                       <button class="btn btn-success btn-level ">Suivant</button>

                   </div>
               </div>
           </div>
        </div>
    </div>

@endsection
