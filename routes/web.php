<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/course',['App\Http\Controllers\LcController', 'index'] )->middleware(['auth', 'CheckTheUserRole'])->name('course');
Route::get('/newlc',['App\Http\Controllers\LcController', 'create'])->middleware(['auth', 'CheckTheUserRole'])->name('newlc');
Route::get('/newitem',['App\Http\Controllers\CourseProduitController', 'newitem'])->middleware(['auth', 'CheckTheUserRole'])->name('newItem');
Route::get('/checkaproduct',['App\Http\Controllers\CourseProduitController', 'checkAProduct'])->middleware(['auth', 'CheckTheUserRole'])->name('checkaproduct');
Route::get('/lc/{id}', ['App\Http\Controllers\LcController', 'oneList'])->middleware(['auth']);


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'CheckTheUserRole'])->name('dashboard');

Route::get('/projet', ['App\\Http\\Controllers\\ProjetController', 'indexAdmin'])->middleware(['auth', 'CheckTheUserRole'])->name('projetAdmin');
Route::get('/client', ['App\Http\Controllers\ClientController', 'index'])->middleware(['auth', 'CheckTheUserRole'])->name('indexClient');
Route::get('/chat', ['App\Http\Controllers\Controller', 'chatIndex'])->middleware(['auth', 'CheckTheUserRole'])->name('chatIndex');
Route::get('/tickets', ['App\Http\Controllers\TicketController', 'ticketsIndex'])->middleware(['auth', 'CheckTheUserRole'])->name('tickets');
Route::get('/produits', ['App\Http\Controllers\ProduitController', 'produitsIndex'])->middleware(['auth', 'CheckTheUserRole'])->name('produits');








//CUD LIVEWIRE ROUTE
Route::get('Projet--create', ['App\Http\Controllers\ProjetController', 'create'])->name('Projet--create');
Route::get('Projet--update', ['App\Http\Controllers\ProjetController', 'update'])->name('Projet--update');
Route::get('Projet--delete', ['App\Http\Controllers\ProjetController', 'delete'])->name('Projet--delete');

Route::get('Client--create', ['App\Http\Controllers\ClientController', 'create'])->name('Client--create');
Route::get('Client--update', ['App\Http\Controllers\ClientController', 'update'])->name('Client--update');
Route::get('Client--delete', ['App\Http\Controllers\ClientController', 'delete'])->name('Client--delete');

Route::get('produit--create', ['App\Http\Controllers\produitController', 'create'])->name('produit--create');
Route::get('produit--update', ['App\Http\Controllers\produitController', 'update'])->name('produit--update');
Route::get('produit--delete', ['App\Http\Controllers\produitController', 'delete'])->name('produit--delete');

Route::post('Ticket--create', ['App\Http\Controllers\TicketController', 'generateTickets'])->name('Ticket--create');


require __DIR__.'/auth.php';
