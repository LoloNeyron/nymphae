<?php

namespace App\Http\Controllers;

use App\Models\produit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class ProduitController extends Controller
{
    public function produitsIndex() {
        $ElementNameSingle = "produit";
        $ElementNameMultiple = "produits";
        $ElementNameSingleMaj = "Produit";
        $ElementNameMultipleMaj = "Produits";

        $lastId = produit::latest()->get('id');
        if(empty($lastId->toArray())){
            $lastId = 1;
        }else{
            $lastId = $lastId[0]->id + 1;
        }

        return view('produits', [
            'lastId' => $lastId,
            'ElementNameSingle' => $ElementNameSingle,
            'ElementNameMultiple' => $ElementNameMultiple,
            'ElementNameSingleMaj' => $ElementNameSingleMaj,
            'ElementNameMultipleMaj' => $ElementNameMultipleMaj,
            ]);
    }

    public function create(Request $request) {
        $product = new produit();
        if($request->input('id')){
            $product->id = $request->input('id');
        }
        if($request->input('created_at_date')){
            $product->created_at = $request->input('created_at_date') . ' ' . $request->input('created_at_time');
        }
        if($request->input('updated_at_date')){
            $product->updated_at = $request->input('updated_at_date') . ' ' . $request->input('updated_at_time');
        }
        $product->nom = $request->input('name');
        $product->prix = $request->input('prix');
        $product->tva = $request->input('tva');
        $product->unite = $request->input('unite');
        $product->description = $request->input('description');

        $product->save();

        return redirect()->back();
    }
}
