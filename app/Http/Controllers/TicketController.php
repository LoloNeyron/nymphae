<?php

namespace App\Http\Controllers;

use App\Models\Tbtn;
use App\Models\Tcontent;
use App\Models\Ticket;
use App\Models\Tordre;
use Illuminate\Http\Request;
use App\Models\projet;
use Illuminate\Support\Facades\DB;

class TicketController extends Controller
{
    public function ticketsIndex(){
        $projects = projet::all();
        $tickets = Ticket::all();
        $toders = DB::table('tordres')->orderBy('order', 'asc')->get();
        $tbtns = Tbtn::all();
        $tcontents = Tcontent::all();
        return view('tickets', ['projects' => $projects, 'tickets' => $tickets, "torders" => $toders, "tbtns" => $tbtns, "tcontents" => $tcontents]);
    }

    public function generateTickets(Request $request){
        if(TicketController::saveBaseTicket($request->input('user_id'),$request->input('idProject'),$request->input('TicketTitle'),$request->input('ticketState'))){
            TicketController::saveSuppTickets(json_decode($request->input('TicketsSupp')));
            return back();
        }else{
            return response()->json([
                'error' => "Erreur d'enregistrement du Ticket"
            ]);
        }
    }

    public static function saveBaseTicket($id_user, $id_projet, $title, $state) {
        $t = new Ticket();
        $t->iduser = $id_user;
        $t->idproject = $id_projet;
        $t->title = $title;
        $t->do = "0";
        $t->state = $state;
        if($t->save()){
            return true;
        }else{
            return false;
        }
    }

    public function saveSuppTickets($inputSupp){
        $lastIdOfTickets = DB::table('tickets')->latest()->first()->id;
        $order = 0;
        foreach ($inputSupp as $element) {
            $order++;
            if($element->type === "text"){
                TcontentController::saveTContent($element->content,$element->class, $lastIdOfTickets,$order);
            }else if($element->type === "btn"){
                TbtnController::saveTicketsBtn($element->title,$element->class,$element->link, $lastIdOfTickets,$order);
            }
        }
    }
}
