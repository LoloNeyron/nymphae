<?php

namespace App\Http\Controllers;

use App\Models\courseProduit;
use Illuminate\Http\Request;

class CourseProduitController extends Controller
{
    public function newitem(Request $request) {
        $item = new courseProduit();

        $item->name = $request->input('name');
        $item->user_id = $request->input('user_id');
        $item->liste_id = $request->input('liste_id');

        $item->save();

        return redirect()->back()->with('message', 'Votre produit a bien été ajouter.')->with('title', 'Ajout')->with('status', 'success');
    }

    public function checkAProduct(Request $request) {
        $item = courseProduit::where('id', $request->input('id'))->first();

//        dd($item);
        if($item->isChecked == 1){
            $item->isChecked = 0;
        }else{
            $item->isChecked = 1;
        }

        $item->save();

        return 'ok';
    }
}
