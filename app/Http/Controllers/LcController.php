<?php

namespace App\Http\Controllers;

use App\Models\courseProduit;
use App\Models\lc;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Jenssegers\Date\Date;

class LcController extends Controller
{
    public function index() {
        Date::setLocale('fr');

//        dd(Date::parse()->format('l j F Y'));

        $lcs = lc::all();

        return view('course', ["lcs" => $lcs]);
    }

    public function oneList($id) {
        $lc = lc::where('id', $id)->first();

        $product = courseProduit::where('liste_id', $id)->get();

        return view('lc', ['lc' => $lc, "product" => $product]);
    }

    public function create(Request $request){
        $lc = new lc();

        $lc->name = $request->input('name');
        $lc->description = $request->input('desc');

        $lc->save();

        return redirect()->back()->with('message', 'Votre nouvelle liste de course a bien été créer')->with('title', 'Création')->with('status', 'success');
    }
}
