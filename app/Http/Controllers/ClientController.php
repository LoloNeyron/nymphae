<?php

namespace App\Http\Controllers;

use App\Models\client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index() {
        $data = client::all();

        $lastId = client::latest()->get('id');
        if(empty($lastId->toArray())){
            $lastId = 1;
        }else{
            $lastId = $lastId[0]->id + 1;
        }

        return view('client', ['data' => $data, 'lastId' => $lastId]);
    }

    public function create(Request $request) {
        $client = new client();
        if($request->input('id')){
            $client->id = $request->input('id');
        }
        if($request->input('created_at_date')){
            $client->created_at = $request->input('created_at_date') . ' ' . $request->input('created_at_time');
        }
        if($request->input('updated_at_date')){
            $client->updated_at = $request->input('updated_at_date') . ' ' . $request->input('updated_at_time');
        }
        $client->name = $request->input('name');
        $client->description = $request->input('description');

        $client->save();

        return redirect()->back()->with('message', 'Le client a bien été créer.')->with('title', 'Succès')->with('status', 'success');
    }

    public function update(Request $request){
        if($request->input('old_id')){
            $client = client::find($request->input('old_id'));
            if($request->input('id') !== $client->id){
                $client->id = $request->input('id');
            }
        }
        $client = client::find($request->input('id'));

        if($request->input('created_at_date') . ' ' . $request->input('created_at_time') !== $client->created_at){
            $client->created_at = $request->input('created_at_date') . ' ' . $request->input('created_at_time');
        }
        if($request->input('updated_at_date') . ' ' . $request->input('updated_at_time') !== $client->updated_at){
            $client->updated_at = $request->input('updated_at_date') . ' ' . $request->input('updated_at_time');
        }
        $client->name = $request->input('name');

        $client->description = $request->input('description');

        $client->save();

        return redirect()->back()->with('message', 'Le client a bien été mis a jour.')->with('title', 'Succès')->with('status', 'success');
    }

    public function delete(Request $request){
        client::destroy($request->input('id'));
        return redirect()->back()->with('message', 'Le client a bien été supprimer.')->with('title', 'Suppression')->with('status', 'classic');
    }
}
