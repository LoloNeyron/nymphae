<?php

namespace App\Http\Controllers;

use App\Models\Tbtn;
use Illuminate\Http\Request;

class TbtnController extends Controller
{
    public static function saveTicketsBtn($title,$class,$link,$ticketid, $order){
        $tbtn = new Tbtn();
        $tbtn->link = $link;
        $tbtn->title = $title;
        $tbtn->class = $class;
        $tbtn->save();
        TordreController::saveOrderElement($ticketid,NULL, $tbtn->id, $order);
        return true;
    }
}
