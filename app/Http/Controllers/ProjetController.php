<?php

namespace App\Http\Controllers;

use App\Models\client;
use App\Models\projet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class ProjetController extends Controller
{
    public function indexAdmin(){
        $colNames = Schema::getColumnListing('projets');
        $colTypes = [];
        foreach($colNames as $col){
            $colTypes[$col] = Schema::getColumnType('projets', $col);
        }

        $data = Projet::all();
        $clients = client::all();
        $lastId = Projet::latest()->get('id');

        if(empty($lastId->toArray())){
            $lastId = 1;
        }else{
            $lastId = $lastId[0]->id + 1;
        }
        return view('projet', ['data' => $data, 'lastId' => $lastId, 'clients' => $clients, "colTypes" => $colTypes]);
    }

    public function create(Request $request) {
        $projet = new projet();
        if($request->input('id')){
            $projet->id = $request->input('id');
        }
        if($request->input('created_at_date')){
            $projet->created_at = $request->input('created_at_date') . ' ' . $request->input('created_at_time');
        }
        if($request->input('updated_at_date')){
            $projet->updated_at = $request->input('updated_at_date') . ' ' . $request->input('updated_at_time');
        }
        $projet->name = $request->input('name');
        $projet->description = $request->input('description');
        $projet->id_client = $request->input('idClient');

        $projet->save();

        return redirect()->back()->with('message', 'Le projet a bien été créer.')->with('title', 'Succès')->with('status', 'success');
    }

    public function update(Request $request){
        $gen = projet::find($request->input('old_id'));
        if($request->input('id') !== $gen->id){
            $gen->id = $request->input('id');
        }
        if($request->input('created_at_date') . ' ' . $request->input('created_at_time') !== $gen->created_at){
            $gen->created_at = $request->input('created_at_date') . ' ' . $request->input('created_at_time');
        }
        if($request->input('updated_at_date') . ' ' . $request->input('updated_at_time') !== $gen->updated_at){
            $gen->updated_at = $request->input('updated_at_date') . ' ' . $request->input('updated_at_time');
        }
        $gen->name = $request->input('name');
//        $gen->code = $request->input('code');

        $gen->save();

        return redirect()->back()->with('message', 'Le projet a bien été mis a jour.')->with('title', 'Succès')->with('status', 'success');
    }

    public function delete(Request $request){
        projet::destroy($request->input('id'));
        return redirect()->back()->with('message', 'Le client a bien été supprimer.')->with('title', 'Suppression')->with('status', 'classic');
    }

}
