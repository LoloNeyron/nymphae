<?php

namespace App\Http\Controllers;

use App\Models\Tcontent;
use Illuminate\Http\Request;

class TcontentController extends Controller
{
    public static function saveTContent($content, $class,$ticketid, $order){
        $tcont = new Tcontent();
        $tcont->content = $content;
        $tcont->class = $class;
        $tcont->save();
        TordreController::saveOrderElement($ticketid,$tcont->id, NULL, $order);
        return true;
    }
}
