<?php

namespace App\Http\Controllers;

use App\Models\Tordre;
use Illuminate\Http\Request;

class TordreController extends Controller
{
    public static function saveOrderElement($tikcetid,$contentid = null,$btnid = null,$order){
        $to = new Tordre();
        $to->id_ticket = $tikcetid;
        $to->id_tcontent = $contentid;
        $to->id_tbtn = $btnid;
        $to->order = $order;
        $to->save();
        return true;
    }
}
