<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckTheUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = User::where('id', Auth::id())->first();
        $route = [];
        if($user->is_appart_user === 1){
            array_push($route, strval("course"));
            array_push($route, strval("newlc"));
            array_push($route, strval("newitem"));
            array_push($route, strval("checkaproduct"));
            array_push($route, 'dashboard');
        }
        if ($user->is_devis_user === 1){
            array_push($route, 'dashboard');
            array_push($route, 'projet');
            array_push($route, 'produits');
            array_push($route, 'client');
        }

        if($user->is_admin === 1){
            return $next($request);
        }else{
            foreach ($route as $r){
                if($r === $request->route()->uri){
                    return $next($request);
                }
            }
            return redirect('/');
        }
    }
}
