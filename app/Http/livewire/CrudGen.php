<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\DB;
use Livewire\Component;

class CrudGen extends Component
{
    public $class;
    public $putScript = false;

    public function render()
    {
        $FullClass = "App\\Models\\" . $this->class;
        $C = new $FullClass;
        $data = $C::all();
        $colsOfData = DB::getSchemaBuilder()->getColumnListing($C->getTable());
        foreach ($colsOfData as $colName){
            $colsType[$colName] = DB::getSchemaBuilder()->getColumnType($C->getTable(), $colName);
        }
        return view('livewire.crud-gen', ["class" => $this->class, "data" => $data, "colsOfData" => $colsType, "putScript" => $this->putScript]);
    }
}
