<?php

namespace App\Http\Livewire;

use App\Models\produit;
use Illuminate\Support\Facades\Schema;
use Livewire\Component;

class ListeItemFromBDD extends Component
{
    public function render()
    {
        $colNames = Schema::getColumnListing('produits');
        $colTypes = [];
        foreach($colNames as $col){
            $colTypes[$col] = Schema::getColumnType('produits', $col);
        }

        $data = produit::all();

        return view('livewire.liste-item-from-b-d-d', ['colTypes' => $colTypes, 'data' => $data]);
    }
}
